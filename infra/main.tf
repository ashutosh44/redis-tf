provider "aws" {
  region = "${var.region}"
  profile = "${var.profile}"
}

module "aws_redis" {
  source              = "../redis"
  cluster_id          = "${var.cluster_id}"
  cluster_description = "${var.cluster_description}"
  node_type           = "${var.node_type}"
  port                = "${var.port}"
  node_count          = "${var.node_count}"
  cluster_sg          = "${var.cluster_sg}"
  client              = "${var.client}"
  parameter_name      = "${var.parameter_name}"
  redis_version       = "${var.redis_version}"
  environment         = "${var.environment}"
  subnet_group_name   = "${var.subnet_group_name}"
  subnet_ids          = "${var.subnet_ids}"
}