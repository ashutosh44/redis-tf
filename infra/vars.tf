variable "region" {
  description = "Region name"
  default     = "ap-south-1"
}

variable "profile" {
  description = "Aws Profiles for Infra"
  default     = "chqbook"
}

variable "client" {
  default = "cashbook_redis_nonprod"
}

variable "cluster_description" {
  default = "redis cluster"
}

variable "cluster_id" {
  default = "tf-redis-cluster-cashbook"
}

variable "environment" {
  default = "dev-env"
}

variable "node_count" {
  default = "2"
}

variable "node_type" {
  default = "cache.t2.micro"
}

variable "port" {
  default = "6379"
}

variable "subnet_group_name" {
  default = "cashbook-nonprod-vpc"
}

variable "subnet_ids" {
  type    = "list"
  default = ["subnet-0366e1d3b03fb539d", "subnet-01e05c331ac8b27e3"]
}

variable "parameter_name" {
  default = "cashbook-tf-param"
}

variable "redis_version" {
  default = "redis5.0"
}

variable "cluster_sg" {
  default = "sg-033cc46de6adb8194"
}