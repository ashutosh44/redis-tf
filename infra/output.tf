output "cluster_endpoint" {
  value = "${module.aws_redis.cluster_endpoint}"
}