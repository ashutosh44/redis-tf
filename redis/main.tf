resource "aws_elasticache_subnet_group" "subnet_group" {
  name       = "${var.subnet_group_name}"
  subnet_ids = "${var.subnet_ids}"
}

resource "aws_elasticache_parameter_group" "default" {
  name   = "${var.parameter_name}"
  family = "redis5.0"
}

resource "aws_elasticache_replication_group" "cluster" {
  replication_group_id          = "${var.cluster_id}"
  replication_group_description = "${var.cluster_description}"
  node_type                     = "${var.node_type}"
  port                          = "${var.port}"
  parameter_group_name          = "${aws_elasticache_parameter_group.default.id}"
  number_cache_clusters         = "${var.node_count}"
  subnet_group_name             = "${aws_elasticache_subnet_group.subnet_group.name}"
  security_group_ids            = ["${var.cluster_sg}"]

  tags = {
    Client      = "${var.client}"
    Environment = "${var.environment}"
    Terraform   = true
  }
}