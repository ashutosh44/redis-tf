variable "client" {
  description = "tag for cluster"
}

variable "cluster_description" {
  description = "cluster description"
}

variable "cluster_id" {
  description = "name of cluster"
}

variable "environment" {
  description = "tag for env"
}

variable "node_count" {
  description = "number of node"
}

variable "node_type" {
  description = "type of node"
}

variable "port" {
  description = "port number of redis"
}

variable "subnet_group_name" {
  description = "group name of redis-subnet"
}

variable "subnet_ids" {
  type        = "list"
  description = "all subnet ids"
}

variable "parameter_name" {
  description = "parameter name"
}

variable "redis_version" {
  description = "redis version"
}

variable "cluster_sg" {
  description = "security group for redis cluster"
}
