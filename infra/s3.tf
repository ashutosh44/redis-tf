terraform {
  backend "s3" {
    bucket  = "chqbok-devops"
    key     = "cashbook-infra/non-prod/cashbook-redis.json"
    region  = "ap-south-1"
  }
}